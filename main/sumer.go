package main

import (
  "fmt"
  "os"
  "log"
  "path/filepath"
  "bufio"
  "strings"
  "unicode"
  "math"
  "strconv"
  "reflect"
)

type Token struct {
  Type string
  Value string
}

type ASTNode struct {
  Type string
  Value string
  Children []ASTNode
}

func main() {
  filePath := os.Args[1]

  interpreter := NewInterpreter()

  interpreter.ExecuteFile(filePath)

  printSlots(interpreter.Slots)
}

func readFile(relativeFilePath string) []string {
  fileLines := []string{}

  // determine file path
  absoluteFilePath, _ := filepath.Abs(relativeFilePath)

  // open file
  codeFile, err := os.Open(absoluteFilePath)
  if err != nil {
    log.Fatal(err)
  }
  defer codeFile.Close()

  // create file scanner
  codeFileScanner := bufio.NewScanner(codeFile)

  // run file scanner
  for codeFileScanner.Scan() {
    fileLines = append(fileLines, codeFileScanner.Text())
  }

  // append EOF code
  fileLines = append(fileLines, "do{}")

  // handle scanner error
  if err := codeFileScanner.Err(); err != nil {
    log.Fatal(err)
  }

  return fileLines
}

var tokenTypes map[string]string = map[string]string {
  "Keyword" : "Keyword",
  "Operator" : "Operator",
  "Seperator" : "Seperator",
  "Identifier" : "Identifier",
  "NumberLiteral" : "NumberLiteral",
  "StringLiteral" : "StringLiteral",
  "BooleanLiteral" : "BooleanLiteral",
}

var tokenTypeValues map[string][]string = map[string][]string {
  "Keyword" : []string{"<-", "do"},
  "Operator" : []string{".", "+", "-", "*", "/", "%", "==", "!=", ">=", "<=", ">", "<", "&&", "||", "for", "if", "where", "on", "~"},
  "Seperator" : []string{"(", ")", "{", "}", "[", "]", ":", ","},
}

var operatorTokenPrecedence map[string]int = map[string]int {
  "~" : 10,
  "on" : 20,
  "where" : 20,
  "for" : 20,
  "if" : 20,
  "&&" : 30,
  "||" : 30,
  "==" : 40,
  "!=" : 40,
  "<=" : 50,
  ">=" : 50,
  ">" : 60,
  "<" : 60,
  "+" : 70,
  "-" : 70,
  "*" : 80,
  "/" : 80,
  "%" : 80,
  "." : 90,
}

func tokenize(codeLines []string) []Token {
  codeTokens := []Token{}

  // find tokens on each line
  for _, codeLine := range codeLines {
    codeLine = codeLine + " " // append EOL character
    codeLineRunes := []rune(codeLine)

    currRuneIndex := 0
    currLexemeBuilder := strings.Builder{} // holder of current lexeme

    // iterate through runes on current line
    for currRuneIndex < len(codeLine) {
      currRune := codeLineRunes[currRuneIndex]

      // TODO ensure space exists between each token

      // handle identifiers ending at current rune
      if currLexemeBuilder.Len() > 0 && unicode.IsSpace(currRune) {

        codeTokens = append(codeTokens, Token{
          Type : tokenTypes["Identifier"],
          Value : currLexemeBuilder.String(),
        })
        currLexemeBuilder.Reset()
        currRuneIndex++
        continue
      }

      // handle comments starting at current rune
      if currRuneIndex + 2 <= len(codeLineRunes) && codeLine[currRuneIndex : currRuneIndex + 2] == "//" {
        
        // handle identifier ending at current rune
        if currLexemeBuilder.Len() > 0 {
          codeTokens = append(codeTokens, Token{
            Type : tokenTypes["Identifier"],
            Value : currLexemeBuilder.String(),
          })
          currLexemeBuilder.Reset()
          currRuneIndex++
        }        

        // stop iterating over runes in current line of code
        break
      }

      // handle keywords starting at current rune
      isKeywordToken := false
      for _, keywordTokenValue := range tokenTypeValues[tokenTypes["Keyword"]] {
        if currRuneIndex + len(keywordTokenValue) <= len(codeLineRunes) - 1 && codeLine[currRuneIndex : currRuneIndex + len(keywordTokenValue)] == keywordTokenValue {
          // handle identifiers
          if currLexemeBuilder.Len() > 0 {
            codeTokens = append(codeTokens, Token{
              Type : tokenTypes["Identifier"],
              Value : currLexemeBuilder.String(),
            })
            currLexemeBuilder.Reset()
          }

          codeTokens = append(codeTokens, Token{
            Type : tokenTypes["Keyword"],
            Value : keywordTokenValue,
          })
          currRuneIndex = currRuneIndex + len(keywordTokenValue)
          isKeywordToken = true
          break
        }
      }
      if isKeywordToken {
        continue
      }

      // handle operators starting at current rune
      isOperatorToken := false
      for _, operatorTokenValue := range tokenTypeValues[tokenTypes["Operator"]] {
        if currRuneIndex + len(operatorTokenValue) <= len(codeLineRunes) - 1 && codeLine[currRuneIndex : currRuneIndex + len(operatorTokenValue)] == operatorTokenValue {
          // handle identifiers
          if currLexemeBuilder.Len() > 0 {
            codeTokens = append(codeTokens, Token{
              Type : tokenTypes["Identifier"],
              Value : currLexemeBuilder.String(),
            })
            currLexemeBuilder.Reset()
          }

          codeTokens = append(codeTokens, Token{
            Type : tokenTypes["Operator"],
            Value : operatorTokenValue,
          })
          currRuneIndex = currRuneIndex + len(operatorTokenValue)
          isOperatorToken = true
          break
        }
      }
      if isOperatorToken {
        continue
      }

      // handle seperator starting at current rune
      isSeperatorToken := false
      for _, seperatorTokenValue := range tokenTypeValues[tokenTypes["Seperator"]] {
        if currRuneIndex + len(seperatorTokenValue) <= len(codeLineRunes) - 1 && codeLine[currRuneIndex : currRuneIndex + len(seperatorTokenValue)] == seperatorTokenValue {
          // handle identifiers
          if currLexemeBuilder.Len() > 0 {
            codeTokens = append(codeTokens, Token{
              Type : tokenTypes["Identifier"],
              Value : currLexemeBuilder.String(),
            })
            currLexemeBuilder.Reset()
          }

          codeTokens = append(codeTokens, Token{
            Type : tokenTypes["Seperator"],
            Value : seperatorTokenValue,
          })
          currRuneIndex = currRuneIndex + len(seperatorTokenValue)
          isSeperatorToken = true
          break
        }
      }
      if isSeperatorToken {
        continue
      }

      // handle string literals starting at current rune
      if currRune == '"' {
        // handle identifiers
        if currLexemeBuilder.Len() > 0 {
          codeTokens = append(codeTokens, Token{
            Type : tokenTypes["Identifier"],
            Value : currLexemeBuilder.String(),
          })
          currLexemeBuilder.Reset()
        }

        stringLiteralEndIndex := len(codeLineRunes) - 1

        // find index of end of string literal
        for codeTokenIndex := currRuneIndex + 1; codeTokenIndex < len(codeLineRunes); codeTokenIndex++ {
          if codeLineRunes[codeTokenIndex] == '"' {
            stringLiteralEndIndex = codeTokenIndex
            break
          }
        }

        codeTokens = append(codeTokens, Token{
          Type : tokenTypes["StringLiteral"],
          Value : codeLine[currRuneIndex + 1 : stringLiteralEndIndex],
        })
        currRuneIndex = stringLiteralEndIndex + 1
        continue
      }

      // handle boolean literals starting at current rune
      if currRuneIndex + len("TRUE") <= len(codeLineRunes) - 1 && codeLine[currRuneIndex : currRuneIndex + len("TRUE")] == "TRUE" {
        // handle identifiers
        if currLexemeBuilder.Len() > 0 {
          codeTokens = append(codeTokens, Token{
            Type : tokenTypes["Identifier"],
            Value : currLexemeBuilder.String(),
          })
          currLexemeBuilder.Reset()
        }

        codeTokens = append(codeTokens, Token{
          Type : tokenTypes["BooleanLiteral"],
          Value : "TRUE",
        })
        currRuneIndex = currRuneIndex + len("TRUE")
        continue
      }
      if currRuneIndex + len("FALSE") <= len(codeLineRunes) - 1 && codeLine[currRuneIndex : currRuneIndex + len("FALSE")] == "FALSE" {
        // handle identifiers
        if currLexemeBuilder.Len() > 0 {
          codeTokens = append(codeTokens, Token{
            Type : tokenTypes["Identifier"],
            Value : currLexemeBuilder.String(),
          })
          currLexemeBuilder.Reset()
        }

        codeTokens = append(codeTokens, Token {
          Type : tokenTypes["BooleanLiteral"],
          Value : "FALSE",
        })
        currRuneIndex = currRuneIndex + len("FALSE")
        continue
      }

      // handle number literals starting at current rune
      if unicode.IsNumber(currRune) {
        // handle identifiers
        if currLexemeBuilder.Len() > 0 {
          codeTokens = append(codeTokens, Token{
            Type : tokenTypes["Identifier"],
            Value : currLexemeBuilder.String(),
          })
          currLexemeBuilder.Reset()
        }

        numberLiteralEndIndex := len(codeLineRunes) - 1

        // find index of end of number literal
        for codeTokenIndex := currRuneIndex + 1; codeTokenIndex < len(codeLineRunes); codeTokenIndex++ {
          if !unicode.IsNumber(codeLineRunes[codeTokenIndex]) && codeLineRunes[codeTokenIndex] != '.' {
            numberLiteralEndIndex = codeTokenIndex - 1
            break
          }
        }

        codeTokens = append(codeTokens, Token{
          Type : tokenTypes["NumberLiteral"],
          Value : codeLine[currRuneIndex : numberLiteralEndIndex + 1],
        })
        currRuneIndex = numberLiteralEndIndex + 1
        continue
      }

      // handle identifiers including current rune
      if unicode.IsSpace(currRune) {
        currLexemeBuilder.Reset()
      } else {
        currLexemeBuilder.WriteRune(currRune)
      }

      // update index of rune in current line
      currRuneIndex++
    }
  }

  return codeTokens
}

var ASTNodeTypes map[string]string = map[string]string {
  "Program" : "Program",
  // commands
  "AssignCommand" : "AssignCommand",
  "DoCommand" : "DoCommand",
  // expressions
  "Expression" : "Expression",
  "Operator" : "Operator",
  "Slot" : "Slot",
  "StringLiteral" : "StringLiteral",
  "NumberLiteral" : "NumberLiteral",
  "BooleanLiteral" : "BooleanLiteral",
  "Table" : "Table",
  "TableElement" : "TableElement",
}

func parse(tokens []Token) []ASTNode {
  commandASTNodes := []ASTNode{}

  codeTokens := []Token{}
  codeTokenIndex := 0

  // depths by different group seperators
  curlyBraceSeperatorDepth := 0
  squareBracketSeperatorDepth := 0
  parenthesesSeperatorDepth := 0

  // starting index of current statement
  statementStartIndex := 0

  // iterate through tokens
  for _, codeToken := range tokens {
    // update current tokens
    codeTokens = append(codeTokens, codeToken)

    // handle start of next statement
    if codeToken.Type == tokenTypes["Keyword"] && curlyBraceSeperatorDepth == 0 && squareBracketSeperatorDepth == 0 && parenthesesSeperatorDepth == 0 && codeTokenIndex - statementStartIndex >= 1 {
      switch codeToken.Value {
      case "<-":
        if (codeTokenIndex - 1) - statementStartIndex >= 1 {
          commandASTNodes = append(commandASTNodes, generateCommandAST(codeTokens[statementStartIndex : codeTokenIndex - 1]))
          statementStartIndex = codeTokenIndex - 1
        }
      case "do":
        commandASTNodes = append(commandASTNodes, generateCommandAST(codeTokens[statementStartIndex : codeTokenIndex]))
        statementStartIndex = codeTokenIndex
      }
    }

    // update depths by group seperators
    if codeToken.Type == tokenTypes["Seperator"] {
      switch codeToken.Value {
      case "{":
        curlyBraceSeperatorDepth++
      case "}":
        curlyBraceSeperatorDepth--
      case "[":
        squareBracketSeperatorDepth++
      case "]":
        squareBracketSeperatorDepth--
      case "(":
        parenthesesSeperatorDepth++
      case ")":
        parenthesesSeperatorDepth--
      }
    }

    codeTokenIndex++

  }  

  return commandASTNodes
}

// generate AST for given tokens of command
func generateCommandAST(codeTokens []Token) ASTNode {
  // depths by different group seperators
  commandCodeCurlyBraceSeperatorDepth := 0
  commandCodeSquareBracketSeperatorDepth := 0
  commandCodeParenthesesSeperatorDepth := 0

  // iterate through tokens in command
  for i, commandCodeToken := range codeTokens {

    // handle keywords indicating command type
    if commandCodeToken.Type == tokenTypes["Keyword"] && commandCodeCurlyBraceSeperatorDepth == 0 && commandCodeSquareBracketSeperatorDepth == 0 && commandCodeParenthesesSeperatorDepth == 0 {
      switch commandCodeToken.Value {
      case "<-":
        return ASTNode{
          Type : ASTNodeTypes["AssignCommand"],
          Value : codeTokens[i - 1].Value,
          Children : []ASTNode{generateExpressionAST(codeTokens[i + 1 : len(codeTokens)])},
        }
      case "do":
        return ASTNode{
          Type : ASTNodeTypes["DoCommand"],
          Children : []ASTNode{generateExpressionAST(codeTokens[i + 1 : len(codeTokens)])},
        }
      }
    }

    // update depths by different grouping seperators
    if commandCodeToken.Type == tokenTypes["Seperator"] {
      switch commandCodeToken.Value {
      case "{":
        commandCodeCurlyBraceSeperatorDepth++
      case "}":
        commandCodeCurlyBraceSeperatorDepth--
      case "[":
        commandCodeSquareBracketSeperatorDepth++
      case "]":
        commandCodeSquareBracketSeperatorDepth--
      case "(":
        commandCodeParenthesesSeperatorDepth++
      case ")":
        commandCodeParenthesesSeperatorDepth--
      }
    }

  }

  return ASTNode{}
}

// generate AST for given tokens of expression
func generateExpressionAST(codeTokens []Token) ASTNode {
  expression := ASTNode{}

  // depths by different seperators ~ 0 at or before start of seperators
  expressionCodeCurlyBraceSeperatorDepth := 0
  expressionCodeSquareBracketSeperatorDepth := 0
  expressionCodeParenthesesSeperatorDepth := 0

  outputASTNodes := []ASTNode{}
  operatorStack := []string{}

  // iterate through each token in expression
  for expressionCodeTokenIndex, expressionCodeToken := range codeTokens {

    // check if group depth is 0
    if expressionCodeCurlyBraceSeperatorDepth == 0 && expressionCodeSquareBracketSeperatorDepth == 0 && expressionCodeParenthesesSeperatorDepth == 0 {
      // push string literals to list of AST nodes
      if expressionCodeToken.Type == tokenTypes["StringLiteral"] {
        outputASTNodes = append(outputASTNodes, ASTNode{
          Type : ASTNodeTypes["StringLiteral"],
          Value : expressionCodeToken.Value,
        })
      }

      // push number literals to list of AST nodes
      if expressionCodeToken.Type == tokenTypes["NumberLiteral"] {
        outputASTNodes = append(outputASTNodes, ASTNode{
          Type : ASTNodeTypes["NumberLiteral"],
          Value : expressionCodeToken.Value,
        })
      }

      // push boolean literals to list of AST nodes
      if expressionCodeToken.Type == tokenTypes["BooleanLiteral"] {
        outputASTNodes = append(outputASTNodes, ASTNode{
          Type : ASTNodeTypes["BooleanLiteral"],
          Value : expressionCodeToken.Value,
        })
      }

      // push identifiers to list of AST nodes
      if expressionCodeToken.Type == tokenTypes["Identifier"] {
        outputASTNodes = append(outputASTNodes, ASTNode{
          Type : ASTNodeTypes["Slot"],
          Value : expressionCodeToken.Value,
        })
      }

      // push seperator delimited groups to list of AST nodes
      if expressionCodeToken.Type == tokenTypes["Seperator"] {
        switch expressionCodeToken.Value {
        case "{":
          curlyBraceCloseIndex := len(codeTokens)
          curlyBraceDepth := 1

          // find index of closing curly brace
          for i := expressionCodeTokenIndex + 1; i < len(codeTokens); i++ {
            if codeTokens[i].Value == "{" {
              curlyBraceDepth++
            } else if codeTokens[i].Value == "}" {
              if curlyBraceDepth == 1 {
                curlyBraceCloseIndex = i
                break
              }

              curlyBraceDepth--
            }
          }

          // generate AST node of program expression
          outputASTNodes = append(outputASTNodes, generateProgramAST(codeTokens[expressionCodeTokenIndex + 1 : curlyBraceCloseIndex]))
        case "[":
          squareBracketCloseIndex := len(codeTokens)
          squareBracketDepth := 1

          // find index of closing square bracket
          for i := expressionCodeTokenIndex + 1; i < len(codeTokens); i++ {
            if codeTokens[i].Value == "[" {
              squareBracketDepth++
            } else if codeTokens[i].Value == "]" {
              if squareBracketDepth == 1 {
                squareBracketCloseIndex = i
                break
              }

              squareBracketDepth--
            }
          }

          // generate AST node of table expression
          outputASTNodes = append(outputASTNodes, generateTableAST(codeTokens[expressionCodeTokenIndex + 1 : squareBracketCloseIndex]))
        case "(":
          parenthesesCloseIndex := len(codeTokens)
          parenthesesDepth := 1

          // find index of closing parenthesis
          for i := expressionCodeTokenIndex + 1; i < len(codeTokens); i++ {
            if codeTokens[i].Value == "(" {
              parenthesesDepth++
            } else if codeTokens[i].Value == ")" {
              if parenthesesDepth == 1 {
                parenthesesCloseIndex = i
                break
              }

              parenthesesDepth--
            }
          }

          // generate AST node of expression of group
          outputASTNodes = append(outputASTNodes, generateExpressionAST(codeTokens[expressionCodeTokenIndex + 1 : parenthesesCloseIndex]))
        }
      } else if expressionCodeToken.Type == tokenTypes["Operator"] {

        // TODO handle unary operators
        if expressionCodeToken.Value == "-"  {

        }

        // push operators of greater precedence from top of stack to queue of output AST nodes
        for len(operatorStack) > 0 && operatorTokenPrecedence[operatorStack[len(operatorStack) - 1]] >= operatorTokenPrecedence[expressionCodeToken.Value] {
          outputASTNodes = append(outputASTNodes, ASTNode{
            Type : tokenTypes["Operator"],
            Value : operatorStack[len(operatorStack) - 1],
          })
          operatorStack = operatorStack[0 : len(operatorStack) - 1]
        }

        // push operator at current index onto stack
        operatorStack = append(operatorStack, expressionCodeToken.Value)
      }
    }

    // update seperator depths
    if expressionCodeToken.Type == tokenTypes["Seperator"] {
      switch expressionCodeToken.Value {
      case "{":
        expressionCodeCurlyBraceSeperatorDepth++
      case "}":
        expressionCodeCurlyBraceSeperatorDepth--
      case "[":
        expressionCodeSquareBracketSeperatorDepth++
      case "]":
        expressionCodeSquareBracketSeperatorDepth--
      case "(":
        expressionCodeParenthesesSeperatorDepth++
      case ")":
        expressionCodeParenthesesSeperatorDepth--
      }
    }

  }

  // handle no operators
  if len(outputASTNodes) == 1 {
    // return expression
    return ASTNode{
      Type : ASTNodeTypes["Expression"],
      Children : []ASTNode{outputASTNodes[0]},
    }
  }

  // handle remaining operators on operator stack
  for len(operatorStack) > 0 {
    outputASTNodes = append(outputASTNodes, ASTNode{
      Type : tokenTypes["Operator"],
      Value : operatorStack[len(operatorStack) - 1],
    })
    operatorStack = operatorStack[0 : len(operatorStack) - 1]
  }

  outputASTNodeIndex := 0
  // iterate through expression AST nodes
  for outputASTNodeIndex < len(outputASTNodes) {
    outputASTNode := outputASTNodes[outputASTNodeIndex]

    // check if output AST node is operator
    if outputASTNode.Type == ASTNodeTypes["Operator"] {
      expression = ASTNode{
        Type : ASTNodeTypes["Operator"],
        Value : outputASTNode.Value,
        Children : []ASTNode{outputASTNodes[outputASTNodeIndex - 2], outputASTNodes[outputASTNodeIndex - 1]},
      }
      prevOutputASTNodes := append(outputASTNodes[0 : outputASTNodeIndex - 2], expression)
      outputASTNodes = append(prevOutputASTNodes, outputASTNodes[outputASTNodeIndex + 1 : len(outputASTNodes)]...)
      outputASTNodeIndex = outputASTNodeIndex - 2
    }

    outputASTNodeIndex++
  }

  return ASTNode{
    Type : ASTNodeTypes["Expression"],
    Children : []ASTNode{expression},
  }
}

// generate AST of program from given list of tokens
func generateProgramAST(codeTokens []Token) ASTNode {
  programAST := ASTNode{
    Type : ASTNodeTypes["Program"],
    Children : []ASTNode{},
  }

  // depths by different seperators
  programCodeCurlyBraceSeperatorDepth := 0
  programCodeSquareBracketSeperatorDepth := 0
  programCodeParenthesesSeperatorDepth := 0

  // index of start of current statement
  programCodeStatementStartIndex := 0

  // iterate through tokens of program code
  for i, programCodeToken := range codeTokens {

    // generate AST of statement ended at current index
    if programCodeToken.Type == tokenTypes["Keyword"] && programCodeCurlyBraceSeperatorDepth == 0 && programCodeSquareBracketSeperatorDepth == 0 && programCodeParenthesesSeperatorDepth == 0 && i - programCodeStatementStartIndex >= 1 {
      switch programCodeToken.Value {
      case "<-":
        if (i - 1) - programCodeStatementStartIndex >= 1 {
          programAST.Children = append(programAST.Children, generateCommandAST(codeTokens[programCodeStatementStartIndex : i - 1]))
          programCodeStatementStartIndex = i - 1
        }
      case "do":
        programAST.Children = append(programAST.Children, generateCommandAST(codeTokens[programCodeStatementStartIndex : i]))
        programCodeStatementStartIndex = i
      }
    } else if i == len(codeTokens) - 1 {
      programAST.Children = append(programAST.Children, generateCommandAST(codeTokens[programCodeStatementStartIndex : len(codeTokens)]))
      programCodeStatementStartIndex = len(codeTokens)
    }

    // update seperator depths
    if programCodeToken.Type == tokenTypes["Seperator"] {
      switch programCodeToken.Value {
      case "{":
        programCodeCurlyBraceSeperatorDepth++
      case "}":
        programCodeCurlyBraceSeperatorDepth--
      case "[":
        programCodeSquareBracketSeperatorDepth++
      case "]":
        programCodeSquareBracketSeperatorDepth--
      case "(":
        programCodeParenthesesSeperatorDepth++
      case ")":
        programCodeParenthesesSeperatorDepth--
      }
    }

  }

  return programAST
}

// generate AST of table from given list of tokens
func generateTableAST(codeTokens []Token) ASTNode {
  tableAST := ASTNode{
    Type : ASTNodeTypes["Table"],
    Children : []ASTNode{},
  }

  // depths by different seperators
  tableCodeCurlyBraceSeperatorDepth := 0
  tableCodeSquareBracketSeperatorDepth := 0
  tableCodeParenthesesSeperatorDepth := 0

  // index of start of current table element
  startTableElementIndex := 0

  currTableElementIndex := 0

  // iterate through tokens of table code
  for i, tableCodeToken := range codeTokens {
    // update seperator depths
    if tableCodeToken.Type == tokenTypes["Seperator"] {
      switch tableCodeToken.Value {
      case "{":
        tableCodeCurlyBraceSeperatorDepth++
      case "}":
        tableCodeCurlyBraceSeperatorDepth--
      case "[":
        tableCodeSquareBracketSeperatorDepth++
      case "]":
        tableCodeSquareBracketSeperatorDepth--
      case "(":
        tableCodeParenthesesSeperatorDepth++
      case ")":
        tableCodeParenthesesSeperatorDepth--
      }
    }

    // generate AST for table element ended at current index
    if ((tableCodeToken.Type == tokenTypes["Seperator"] && tableCodeToken.Value == ",") || i == len(codeTokens) - 1) && tableCodeCurlyBraceSeperatorDepth == 0 && tableCodeSquareBracketSeperatorDepth == 0 && tableCodeParenthesesSeperatorDepth == 0 {
      if i == len(codeTokens) - 1 {
        tableAST.Children = append(tableAST.Children, generateTableElementAST(codeTokens[startTableElementIndex : i + 1], currTableElementIndex))
        startTableElementIndex = i + 1
        currTableElementIndex--
      } else {
        tableAST.Children = append(tableAST.Children, generateTableElementAST(codeTokens[startTableElementIndex : i], currTableElementIndex))
        startTableElementIndex = i + 1
        currTableElementIndex--
      }
    }

  }

  return tableAST
}

// generate AST for table element from given list of tokens
func generateTableElementAST(codeTokens []Token, tableElementIndex int) ASTNode {
  // depths by different seperators
  tableElementCodeCurlyBraceSeperatorDepth := 0
  tableElementCodeSquareBracketSeperatorDepth := 0
  tableElementCodeParenthesesSeperatorDepth := 0

  // iterate through tokens of table element code
  for i, tableElementCodeToken := range codeTokens {
    // update seperator depths
    if tableElementCodeToken.Type == tokenTypes["Seperator"] {
      switch tableElementCodeToken.Value {
      case "{":
        tableElementCodeCurlyBraceSeperatorDepth++
      case "}":
        tableElementCodeCurlyBraceSeperatorDepth--
      case "[":
        tableElementCodeSquareBracketSeperatorDepth++
      case "]":
        tableElementCodeSquareBracketSeperatorDepth--
      case "(":
        tableElementCodeParenthesesSeperatorDepth++
      case ")":
        tableElementCodeParenthesesSeperatorDepth--
      }
    }

    // generate AST of table element
    if tableElementCodeToken.Type == tokenTypes["Seperator"] && tableElementCodeToken.Value == ":" && tableElementCodeCurlyBraceSeperatorDepth == 0 && tableElementCodeSquareBracketSeperatorDepth == 0 && tableElementCodeParenthesesSeperatorDepth == 0 {
      return ASTNode{
        Type : ASTNodeTypes["TableElement"],
        Value : codeTokens[i - 1].Value,
        Children : []ASTNode{generateExpressionAST(codeTokens[i + 1 : len(codeTokens)])},
      }
    }
  }

  return ASTNode{
    Type : ASTNodeTypes["TableElement"],
    Value : string(tableElementIndex),
    Children : []ASTNode{generateExpressionAST(codeTokens[0 : len(codeTokens)])},
  }
}

type Program struct {
  Slots map[string]Value
  CommandExpressions []ASTNode
  ForExpression ASTNode
  IfExpression ASTNode
}

var ValueTypes map[string]string = map[string]string{
  "String" : "String",
  "Float64" : "Float64",
  "Boolean" : "Boolean",
  "Table" : "Table",
  "Program" : "Program",
}

type Value struct {
  ValueType string
  StringValue string
  Float64Value float64
  BooleanValue bool
  TableValue map[string]Value
  ProgramValue Program
}

func interpret(commandASTNodes []ASTNode, initialSlots map[string]Value) map[string]Value {
  programSlots := initialSlots

  // execute each command
  for _, commandExpression := range commandASTNodes {
    switch commandExpression.Type {
    case ASTNodeTypes["AssignCommand"]:
      programSlots = executeAssignCommand(commandExpression, programSlots)
    case ASTNodeTypes["DoCommand"]:
      programSlots = executeDoCommand(commandExpression, programSlots)
    }
  }  

  return programSlots
}

// executes a program given list of command expression AST nodes, initial slots, and the slots the program is executed in
// returns the updated slots
func executeProgram(commandExpressionNodes []ASTNode, initialProgramSlots map[string]Value, slots map[string]Value) map[string]Value {
  programSlots := initialProgramSlots

  // populate program slots with slots of program executed in
  for slotName, slotValue := range slots {
    programSlots[slotName] = slotValue
  }

  // execute program commands
  for _, commandExpression := range commandExpressionNodes {
    switch commandExpression.Type {
    case ASTNodeTypes["AssignCommand"]:
      programSlots = executeAssignCommand(commandExpression, programSlots)
    case ASTNodeTypes["DoCommand"]:
      programSlots = executeDoCommand(commandExpression, programSlots)
    }
  }

  // update slots of program is executed in
  for programSlotName, programSlotValue := range programSlots {
    slots[programSlotName] = programSlotValue
  }

  // return updated slots of program executed in
  return slots
}

// executes an assign command given the AST of the command expression and the slots of program the command is executed in
// returns the updated slots
func executeAssignCommand(assignCommandExpression ASTNode, slots map[string]Value) map[string]Value {
  slotName := assignCommandExpression.Value
  slotValue := evaluateExpression(assignCommandExpression.Children[0], slots)

  slots[slotName] = slotValue

  return slots
}

// executes a do command given the AST of the command expression and the slots of program the command is executed in
// returns the updated slots
func executeDoCommand(doCommandExpression ASTNode, slots map[string]Value) map[string]Value {
  programValue := evaluateExpression(doCommandExpression.Children[0], slots).ProgramValue

  ifValue := true
  forValue := true

  if !reflect.DeepEqual(programValue.IfExpression, ASTNode{}) {
    ifValue = evaluateExpression(programValue.IfExpression, slots).BooleanValue
  }

  if !reflect.DeepEqual(programValue.ForExpression, ASTNode{}) {
    forValue = evaluateExpression(programValue.ForExpression, slots).BooleanValue
  }

  if ifValue == true {
    // repeat as long as for expression evaluates to true
    for forValue == true {
      // update slots
      slots = executeProgram(programValue.CommandExpressions, programValue.Slots, slots)

      // update value of for expression
      if !reflect.DeepEqual(programValue.ForExpression, ASTNode{}) {
        forValue = evaluateExpression(programValue.ForExpression, slots).BooleanValue
      } else {
        forValue = false
      }
    }
  }

  // return updated slots of program command is executed in
  return slots
}


// returns the Value of an expressions given the AST of the expression and the slots of program the expression is executed in
func evaluateExpression(expression ASTNode, slots map[string]Value) Value {
  switch expression.Type {
  case ASTNodeTypes["Expression"]:
    return evaluateExpression(expression.Children[0], slots)
  case ASTNodeTypes["Slot"]:
    slotName := expression.Value
    slotValue, _ := slots[slotName]

    return slotValue
  case ASTNodeTypes["StringLiteral"]:
    return Value{
      ValueType : ValueTypes["String"],
      StringValue : expression.Value,
    }
  case ASTNodeTypes["NumberLiteral"]:
    float64Value, _ := strconv.ParseFloat(expression.Value, 64)

    return Value{
      ValueType : ValueTypes["Float64"],
      Float64Value : float64Value,
    }
  case ASTNodeTypes["BooleanLiteral"]:
    booleanValue, _ := strconv.ParseBool(expression.Value)

    return Value{
      ValueType : ValueTypes["Boolean"],
      BooleanValue : booleanValue,
    }
  case ASTNodeTypes["Table"]:
    tableValues := map[string]Value{}

    // evaluate each table element value
    for _, tableElement := range expression.Children {
      tableValues[tableElement.Value] = evaluateExpression(tableElement.Children[0], slots)
    }

    return Value{
      ValueType : ValueTypes["Table"],
      TableValue : tableValues,
    }
  case ASTNodeTypes["Program"]:
    // program value with ASTs of commands and initially false for expression
    programValue := Program{
      CommandExpressions : expression.Children,
      Slots : map[string]Value{},
      IfExpression : ASTNode{},
      ForExpression : ASTNode{},
    }

    return Value{
      ValueType : ValueTypes["Program"],
      ProgramValue : programValue,
    }
  case ASTNodeTypes["Operator"]:
    operandExpressions := []ASTNode{expression.Children[0], expression.Children[1]}
    operandValues := []Value{evaluateExpression(expression.Children[0], slots), evaluateExpression(expression.Children[1], slots)}

    // handle operands of same type
    if operandValues[0].ValueType == operandValues[1].ValueType {
      operandsValueType := operandValues[0].ValueType

      switch operandsValueType {
      case ValueTypes["String"]:
        switch expression.Value {
        case "+":
          return Value{
            ValueType : ValueTypes["String"],
            StringValue : operandValues[0].StringValue + operandValues[1].StringValue,
          }
        case "==":
          return Value{
            ValueType : ValueTypes["Boolean"],
            BooleanValue : operandValues[0].StringValue == operandValues[1].StringValue,
          }
        case "!=":
          return Value{
            ValueType : ValueTypes["Boolean"],
            BooleanValue : operandValues[0].StringValue != operandValues[1].StringValue,
          }
        }
      case ValueTypes["Float64"]:
        switch expression.Value {
        case "+":
          return Value{
            ValueType : ValueTypes["Float64"],
            Float64Value : operandValues[0].Float64Value + operandValues[1].Float64Value,
          }
        case "-":
          return Value{
            ValueType : ValueTypes["Float64"],
            Float64Value : operandValues[0].Float64Value - operandValues[1].Float64Value,
          }
        case "*":
          return Value{
            ValueType : ValueTypes["Float64"],
            Float64Value : operandValues[0].Float64Value * operandValues[1].Float64Value,
          }
        case "/":
          return Value{
            ValueType : ValueTypes["Float64"],
            Float64Value : operandValues[0].Float64Value / operandValues[1].Float64Value,
          }
        case "%":
          return Value{
            ValueType : ValueTypes["Float64"],
            Float64Value : math.Mod(operandValues[0].Float64Value, operandValues[1].Float64Value),
          }
        case "==":
          return Value{
            ValueType : ValueTypes["Boolean"],
            BooleanValue : operandValues[0].Float64Value == operandValues[1].Float64Value,
          }
        case "!=":
          return Value{
            ValueType : ValueTypes["Boolean"],
            BooleanValue : operandValues[0].Float64Value != operandValues[1].Float64Value,
          }
        case ">":
          return Value{
            ValueType : ValueTypes["Boolean"],
            BooleanValue : operandValues[0].Float64Value > operandValues[1].Float64Value,
          }
        case "<":
          return Value{
            ValueType : ValueTypes["Boolean"],
            BooleanValue : operandValues[0].Float64Value < operandValues[1].Float64Value,
          }
        case ">=":
          return Value{
            ValueType : ValueTypes["Boolean"],
            BooleanValue : operandValues[0].Float64Value >= operandValues[1].Float64Value,
          }
        case "<=":
          return Value{
            ValueType : ValueTypes["Boolean"],
            BooleanValue : operandValues[0].Float64Value <= operandValues[1].Float64Value,
          }
        }
      case ValueTypes["Boolean"]:
        switch expression.Value {
        case "==":
          return Value{
            ValueType : ValueTypes["Boolean"],
            BooleanValue : operandValues[0].BooleanValue == operandValues[1].BooleanValue,
          }
        case "!=":
          return Value{
            ValueType : ValueTypes["Boolean"],
            BooleanValue : operandValues[0].BooleanValue != operandValues[1].BooleanValue,
          }
        case "&&":
          return Value{
            ValueType : ValueTypes["Boolean"],
            BooleanValue : operandValues[0].BooleanValue && operandValues[1].BooleanValue,
          }
        case "||":
          return Value{
            ValueType : ValueTypes["Boolean"],
            BooleanValue : operandValues[0].BooleanValue || operandValues[1].BooleanValue,
          }
        }
      }
    }

    // handle operands of different type
    if operandValues[0].ValueType != operandValues[1].ValueType {
      switch expression.Value {
      case "for":
        programValue := operandValues[0].ProgramValue
        programValue.ForExpression = operandExpressions[1]

        return Value{
          ValueType : ValueTypes["Program"],
          ProgramValue : programValue,
        }
      case "if":
        programValue := operandValues[0].ProgramValue
        programValue.IfExpression = operandExpressions[1]

        return Value{
          ValueType : ValueTypes["Program"],
          ProgramValue : programValue,
        }
      case "where":
        programValue := operandValues[0].ProgramValue
        newProgramSlots := operandValues[1].TableValue

        // modify slots of program with mappings from map
        for newProgramSlotName, newProgramSlotValue := range newProgramSlots {
          programValue.Slots[newProgramSlotName] = newProgramSlotValue
        }

        return Value{
          ValueType : ValueTypes["Program"],
          ProgramValue : programValue,
        }
      case ".":
        switch operandValues[0].ValueType {
        case ValueTypes["Table"]:
          tableElementValues := operandValues[0].TableValue
          tableElementName := operandExpressions[1].Value

          return tableElementValues[tableElementName]
        case ValueTypes["Program"]:
          programSlots := operandValues[0].ProgramValue.Slots
          programSlotName := operandExpressions[1].Value

          return programSlots[programSlotName]
        }

        return Value{}
      }

    }

  }

  return Value{}
}

var interpreterStates map[string]string = map[string]string{
  "Running" : "Running",
  "Stopped" : "Stopped",
}

type Interpreter struct {
  Slots map[string]Value
  State string
}

func (interpreter Interpreter) IsRunning() bool {
  if interpreter.State == interpreterStates["Running"] {
    return true
  }

  return false
}

func (interpreter Interpreter) IsStopped() bool {
  if interpreter.State == interpreterStates["Stopped"] {
    return true
  }

  return false
}

func (interpreter *Interpreter) ExecuteLines(codeLines []string) {
  interpreter.State = interpreterStates["Running"]

  codeTokens := tokenize(codeLines)
  commandASTNodes := parse(codeTokens)
  interpreter.Slots = interpret(commandASTNodes, interpreter.Slots)

  interpreter.State = interpreterStates["Stopped"]
}

func (interpreter *Interpreter) ExecuteFile(filePath string) {
  interpreter.State = interpreterStates["Running"]

  codeLines := readFile(filePath)
  codeTokens := tokenize(codeLines)
  commandASTNodes := parse(codeTokens)
  interpreter.Slots = interpret(commandASTNodes, interpreter.Slots)

  interpreter.State = interpreterStates["Stopped"]
}

func NewInterpreter() *Interpreter {
  newInterpreter := new(Interpreter)
  newInterpreter.Slots = map[string]Value{}
  newInterpreter.State = interpreterStates["Stopped"]

  return newInterpreter
}

// handle an error
func handleError(errorMessage string) {
  fmt.Println(errorMessage)
}

// pretty prints an AST
func printAST(inputASTNode ASTNode) {
	printASTFromLevel(inputASTNode, 0)
}

func printASTFromLevel(inputASTNode ASTNode, level int) {
	for codeTokenIndex := 0; codeTokenIndex < level; codeTokenIndex++ {
		fmt.Print(" ")
	}
	fmt.Print("|_")
	fmt.Println(inputASTNode.Type + "," + strings.Replace(inputASTNode.Value, " ", "_", -1))

	for _, inputASTNodeChild := range inputASTNode.Children {
		printASTFromLevel(inputASTNodeChild, level + 1)
	}
}

func printSlots(slots map[string]Value) {
  for slotName, slotValue := range slots {
    fmt.Print(slotName + " > " )
    printValue(slotValue)
    fmt.Println()
  }
}

func printValue(value Value) {
  switch value.ValueType {
  case ValueTypes["String"]:
    fmt.Print(value.StringValue)
  case ValueTypes["Boolean"]:
    fmt.Printf("%t", value.BooleanValue)
  case ValueTypes["Float64"]:
    fmt.Printf("%f", value.Float64Value)
  case ValueTypes["Table"]:
    fmt.Print("a table with ")
    fmt.Print(len(value.TableValue))
    fmt.Print(" elements")
  case ValueTypes["Program"]:
    fmt.Print("a program with")
    fmt.Print(len(value.ProgramValue.CommandExpressions))
    fmt.Print(" command expressions")
  }
}