This is a programming language designed to have minimal constructs and be easily embeddable in host applications. There are really just 4 important constructs in the language : programs, commands, expressions, operators.

# Programs

Programs are essentially state machines. They are defined by a sequence of Commands and empty Slots. The sequence of commands in a `.smr` file defines a Program with no Slots. Programs are also Expressions and can be expressed as follows -

```
{
    // program code goes here
}
```

# Commands

Commands are executed in order by the interpreter. There are two basic commands : the Assign Command and the Do Command. The Assign Command assigns a value to slot.

```
total <- 50 + 9.99 + 0.49
```

The left half of the command is the name of the slot and the right half contains the Expression that is evaluated and put in the given slot.

The Do Command executes a Program.

```
do {
    total <- total * 0.85
}
```

The right half of the command is the Expression that is evaluated and executed.

# Expressions

There are 5 Simple Expressions : Numbers, Strings, Booleans, Tables, and Programs. Numbers can be either integers or floating point numbers.

```
5 // an integer
3.1456 // a floating point number
```

Strings are strings.

```
"hello world" // a string
```

Booleans have two possible values : true and false.

```
true
false
```

Tables express mappings to values. The following table maps `0` to `"Bob"` and `1` to `"Alice"`.

```
[
    "Alice",
    "Bob"
]
```

Tables can also map to values from custom identifiers.

```
[
    health : 90,
    damage : 5,
    speed : 7.5
]
```

Programs are defined by a sequence of Commands.

```
{
    health <- health - 5
    damage <- 10
}
```

# Operators

There are 20 operators:

```
+ - * / %
== != >= <= > <
&& ||
if for
where
.
```

